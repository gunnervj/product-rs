var express = require('express');
var bodyParser = require('body-parser');
var productRouter = new express.Router();

var Product = require('../../model/product');


productRouter.use(bodyParser.json());
productRouter.use(bodyParser.urlencoded({extended: true}));


productRouter.get('/products', (request, response) => {
    console.log('getting products');
    Product.find({})
            .select("-_id -__v")
            .then( (product) => {
                response.status(200).send({ "message" : "Success", "product" : product});
            }).catch( (e) => {
                console.log(e);
                response.status(500).send({error: 'something went wrong..'})
            });

});

productRouter.get('/products/:name', (request, response) => {
    console.log('getting products');
    Product.findOne({name: request.params.name})
            .select("-_id -__v")
            .then((product) => { 
                if (product) {
                    return response.status(200).send({ "message" : "Success", "product" : product});
                } else {
                    return response.status(404).send({ "message" : "Product not found.."});
                }
            }).catch( (err) => {
                console.log(err);
                return response.status(500).send({"message" : "Something went wrong.."});
            });

});

productRouter.post('/products', (request, response) => {
        Product.create({
            name: request.body.name,
            description: request.body.description,
            price: request.body.price    
        })
        .then( (product) => {
            console.log("Created Product || " + product);
            return response.status(201).send({ "message" : "Success", "product" : { name: product.name, description: product.description, price: product.price}});
        }).catch( (err) => {
            if (err.code === 11000) {
                return response.status(400).send({message: 'Product already exists..'});
            } else {
                console.log(err);
                return response.status(500).send({message: 'something went wrong..'});
            }
        });
});

productRouter.delete('/products/:name', async (request, response) => {
    try {
        const product =  await Product.findOneAndDelete({name: request.params.name});
        if (product) {
            return response.status(200).send({ "message" : "Success", "product" : { name: product.name}});
        } else {
            return response.status(400).send({message: 'Product Not Found..'});
        }
    } catch(e) {
        console.log(e);
        return response.status(500).send({message: 'something went wrong..'});
    }  
});


productRouter.put('/products/:name', async (request, response) => {
    try {
        const updateKeysOnRequest = Object.keys(request.body);
        const updateAllowedKeys = ['description', 'price'];

        const isValidUpdate = updateKeysOnRequest.every((updateKey) => updateAllowedKeys.includes(updateKey));

        if (!isValidUpdate) {
            return response.status(400).send({message: 'Invalid Updates. Can update only Description and Price'});
        }

        const product =  await Product.findOneAndUpdate({name: request.params.name}, request.body);
        if (product) {
            return response.status(200).send({ "message" : "Success", "product" : { name: product.name}});
        } else {
            return response.status(400).send({message: 'Product Not Found..'});
        }
    } catch(e) {
        console.log(e);
        return response.status(500).send({message: 'something went wrong..'});
    }  
});



module.exports = productRouter;