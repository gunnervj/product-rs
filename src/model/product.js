var mongoose = require('mongoose');
var validator = require('validator');

var productSchema = mongoose.Schema({
    name: { 
        type: String,
        required: true,
        trim: true,
        unique: true,
        index: true
    },
    description : {
        type: String,
        trim: true
    },
    price: {
        type: Number,
        required: true
    }
});

var Product = mongoose.model('Product', productSchema);

module.exports = Product;
