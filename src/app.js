const express = require('express');
const app = express();

require('./config/mongoose')

var productController = require('./controller/product/productController')
app.use(productController);

app.listen(3000, () => {
    console.log('Express started at port 3000');
})